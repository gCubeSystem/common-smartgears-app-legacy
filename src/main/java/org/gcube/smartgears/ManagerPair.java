package org.gcube.smartgears;

import java.util.concurrent.Future;

public class ManagerPair {

	private Class<? extends ApplicationManager> clazz;
	
	private Future<ApplicationManager> future;

	public ManagerPair(Class<? extends ApplicationManager> clazz, Future<ApplicationManager> future) {
		super();
		this.clazz = clazz;
		this.future = future;
	}

	public Class<? extends ApplicationManager> getImplementationClass() {
		return clazz;
	}

	public Future<ApplicationManager> getFuture() {
		return future;
	}
	
	
}

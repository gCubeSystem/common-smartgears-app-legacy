package org.gcube.smartgears.application.manager;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ApplicationManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javassist.util.proxy.MethodHandler;



public class OnlineProvider extends ApplicationManagerProvider {

	private static Logger logger = LoggerFactory.getLogger(OnlineProvider.class); 
	
	private Map<String, Map<String, Future<ApplicationManager>>> appManagerMap = new HashMap<String, Map<String, Future<ApplicationManager>>>(); 

	private OnlineObserver observer = new OnlineObserver(this);
	
	@Override
	protected Future<ApplicationManager> retrieveFuture(Class<? extends ApplicationManager> applicationManagerClass) {
		return 	appManagerMap.get(applicationManagerClass.getCanonicalName()).get(ScopeProvider.instance.get());
	}

	@Override
	protected MethodHandler getMethdoHandler(Class<? extends ApplicationManager> applicationManagerClass) {
		MethodHandler handler = new MethodHandler() {
		    @Override
		    public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
		    	if (ScopeProvider.instance.get()==null) throw new RuntimeException("error invoking application manager method, scope is not set in this thread");
		    	logger.debug("applicationManagerClass is {}",applicationManagerClass.getCanonicalName());
		    	Future<ApplicationManager> appManagerFuture = retrieveFuture(applicationManagerClass);
				logger.debug("appmanager future is null? {}", appManagerFuture==null);
				logger.debug("thisMethod is null? {}", thisMethod==null);
		    	return thisMethod.invoke(appManagerFuture.get(), args);
		    }
		};
		return handler;
	}
	
	public Map<String, Map<String, Future<ApplicationManager>>> getAppmanagerMap(){
		return appManagerMap;
	}

	@Override
	protected AppManagerObserver getObserver() {
		return observer;
	}
	
}

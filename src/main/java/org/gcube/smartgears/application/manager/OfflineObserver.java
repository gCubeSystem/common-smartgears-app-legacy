package org.gcube.smartgears.application.manager;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.gcube.common.events.Observes;
import org.gcube.common.events.Observes.Kind;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.Constants;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineObserver implements AppManagerObserver  {

	private static Logger log = LoggerFactory.getLogger(OfflineObserver.class);

	private static ExecutorService service = Executors.newCachedThreadPool();

	private Set<Class<? extends ApplicationManager>> managerClasses ;
	
	private OfflineProvider provider;

	public OfflineObserver(OfflineProvider provider) {
		this.provider = provider;
	}

	public void register() {
		this.onRegistration(null);
	}

	
	@Override
	@Observes(value=Constants.token_registered, kind=Kind.safe)
	public synchronized void  onRegistration(String parameter){
		log.info("offline registration");
		for (Class<? extends ApplicationManager> appManager: managerClasses){
			Future<ApplicationManager> appManagerFuture = service.submit(new OfflineInitAppManager(appManager));
			provider.getAppmanagerMap().put(appManager.getCanonicalName(), appManagerFuture);
		}
	}

	@Override
	@Observes(value=Constants.token_removed, kind=Kind.critical)
	public synchronized void onRemove(final String securityToken){

	}

	@Override
	public synchronized void  onStop(ApplicationContext appContext){
		provider.getAppmanagerMap().values().forEach( v -> {
			try {
				v.get().onShutdown();
			} catch (InterruptedException | ExecutionException e) {
				log.warn("error shutting down appmanager ");
				
			}
		});
		unregister();
	}

	@Override
	public void unregister(){
		service.shutdownNow();
	}

	public class OfflineInitAppManager implements Callable<ApplicationManager>{

		private Class<? extends ApplicationManager> managerClass;

		public OfflineInitAppManager(Class<? extends ApplicationManager> managerClass){
			this.managerClass = managerClass;
		}

		@Override
		public ApplicationManager call() throws Exception {
			ApplicationManager manager = managerClass.newInstance();
			try {
				log.info("calling on onInit of {}",manager.getClass().getCanonicalName());
				manager.onInit();
			} catch (Exception e) {
				log.warn("error on onInit of {}",manager.getClass().getCanonicalName(), e);
			} 
			return manager;
		}
	}

	public class OfflineShutDownAppManager implements Runnable{

		private Future<ApplicationManager> appManager;

		public OfflineShutDownAppManager(Future<ApplicationManager> appManager){
			this.appManager = appManager;
		}

		@Override
		public void run() {
			try {
				log.info("calling on ShutDown of {} ",appManager.getClass().getCanonicalName());
				appManager.get().onShutdown();
			} catch (Exception e) {
				log.warn("error on onShutdown of {} ",appManager.getClass().getCanonicalName(), e);
			}
		}
	}

	@Override
	public void setStartingTokens(Collection<String> startingTokens) {		
	}

	@Override
	public void setApplicationManagerClasses(Set<Class<? extends ApplicationManager>> managerClasses) {
		this.managerClasses = managerClasses;		
	}
}


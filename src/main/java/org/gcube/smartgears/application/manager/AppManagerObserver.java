package org.gcube.smartgears.application.manager;

import java.util.Collection;
import java.util.Set;

import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.context.application.ApplicationContext;

public interface AppManagerObserver {

	void onRegistration(String parameter);

	void onRemove(String securityToken);

	void onStop(ApplicationContext appContext);

	void unregister();
	
	void setStartingTokens(Collection<String> startingTokens);
	
	void setApplicationManagerClasses(Set<Class<? extends ApplicationManager>> managersClasses);
	
	public void register();

}